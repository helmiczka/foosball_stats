import sys
import re
from trueskill import TrueSkill, Rating, rate, BETA
import trueskill
import itertools
import math


# env = TrueSkill(draw_probability=0.0)
# env.make_as_global()


class Player:

    def __init__(self):
        self.games = 1
        self.skill = Rating()

    def rank(self):
        return self.skill.mu - 3*self.skill.sigma

class Match:

    def __init__(self, line, players):
        self.PA1, self.PA2, self.GA, self.GB, self.PB1, self.PB2 = line.split(',')[0:6]
        # print(self.PA1, self.PA2, ' -:- ', self.GA, ':', self.GB, ' -:- ', self.PB1, self.PB2)
        self.GA = int(self.GA)
        self.GB = int(self.GB)
        for player in [self.PA1, self.PA2, self.PB1, self.PB2]:
            if player in players:
                players[player].games += 1
            else:
                players[player] = Player()
            
    
    def recalculate_skills(self, players):
        team_A = [players[self.PA1].skill, players[self.PA2].skill]
        team_B = [players[self.PB1].skill, players[self.PB2].skill]
        
        ranks = [0, 1]
        # if team_B won, swap ranks (lower number means victory)
        if self.GB > self.GA:
            ranks = [1, 0]
        
        (new_PA1_rating, new_PA2_rating), (new_PB1_rating, new_PB2_rating) = rate([team_A, team_B], ranks=ranks)
        players[self.PA1].skill = new_PA1_rating
        players[self.PA2].skill = new_PA2_rating
        players[self.PB1].skill = new_PB1_rating
        players[self.PB2].skill = new_PB2_rating


def line_contains_match_result(line):
    pattern = re.compile("^\D+,\D+,\d+,\d+,\D+,\D+")
    return pattern.match(line)


class ExtendedStats():

    def __init__(self):
        self.players = {}
        self.players_history = {}
        self.match_history = []
        self.total_match_count = 0


    def read_file(self, file):
        f = open(sys.argv[1], mode="r", encoding="utf-8")
        self.parse_file(f.readlines())


    def parse_file(self, lines):
        self.file_lines = lines
        # get players names
        for line in self.file_lines:
            if line_contains_match_result(line):
                self.add_names(line)
                self.total_match_count += 1
        
        for line in self.file_lines:
            if line_contains_match_result(line):
                match = Match(line, self.players)
                match.recalculate_skills(self.players)
                self.make_skills_snapshot(match)
                self.match_history.append(match)
    
    
    def add_names(self, line):
        PA1, PA2, _, _, PB1, PB2 = line.split(',')[0:6]
        if not PA1 in self.players_history:
            self.players_history[PA1] = [0]
        if not PA2 in self.players_history:
            self.players_history[PA2] = [0]
        if not PB1 in self.players_history:
            self.players_history[PB1] = [0]
        if not PB2 in self.players_history:
            self.players_history[PB2] = [0]


    def make_skills_snapshot(self, match):
        for p in self.players_history:
            # duplicate last record
            self.players_history[p].append(self.players_history[p][-1])
            # if the player played the match, update it with new value
        self.players_history[match.PA1][-1] = self.players[match.PA1].rank()
        self.players_history[match.PA2][-1] = self.players[match.PA2].rank()
        self.players_history[match.PB1][-1] = self.players[match.PB1].rank()
        self.players_history[match.PB2][-1] = self.players[match.PB2].rank()
    
    
    def win_probability(team1, team2):
        delta_mu = sum(r.mu for r in team1) - sum(r.mu for r in team2)
        sum_sigma = sum(r.sigma ** 2 for r in itertools.chain(team1, team2))
        size = len(team1) + len(team2)
        denom = math.sqrt(size * (BETA * BETA) + sum_sigma)
        ts = trueskill.global_env()
        return ts.cdf(delta_mu / denom)
    
    
    def real_results(self, team1, team2):
        for line in self.file_lines:
            if line_contains_match_result(line):
                PA1, PA2, _, _, PB1, PB2 = line.split(',')[0:6]
                if (((PA1 in team1 and PA2 in team1) and (PB1 in team2 and PB2 in team2))
                    or
                    ((PA1 in team2 and PA2 in team2) and (PB1 in team1 and PB2 in team1))):
                    return(line)
        return "no matches"
    
    
    def print_history(self):
        for p in self.players_history:
            print(p,',',end='')
        print()
        
        # print to screen
        for i in range(0, self.total_match_count+1):
            for p in self.players_history:
                print(round(self.players_history[p][i], 1),',',end='')
            print()
        
        # print to file
        with open('skill_history.csv', mode="w", encoding="utf-8") as fw:
            for p in self.players_history:
                print(p,',',end='', file=fw)
            print(file=fw)
            
            for i in range(0, self.total_match_count+1):
                for p in self.players_history:
                    print(self.players_history[p][i],',',end='', file=fw)
                print(file=fw)


if __name__ == "__main__":
    stats = ExtendedStats()
    stats.read_file(sys.argv[1])

    # print('\nName \t games \t skill\n')
    # for player, info in players.items():
        # print(player, '\t', info.games, '\t', info.skill)

    print('\nSorted by TrueSkill rank\nName \t games \t rank\n')
    for player, info in sorted(stats.players.items(), key = lambda name: stats.players[name[0]].rank(), reverse=True):
        print(player, '\t', info.games, '\t', round(info.rank(), 1), info.skill)

    stats.print_history()
    
    print(ExtendedStats.win_probability(
                          [stats.players['CAN'].skill, stats.players['CAN'].skill],
                          [stats.players['FIN'].skill, stats.players['FIN'].skill])
                          )
    
    
    print(stats.real_results(
                          ['CAN', 'CAN'],
                          ['FIN', 'FIN'])
                          )
